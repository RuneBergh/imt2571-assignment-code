<?php
include_once("IModel.php");
include_once("Book.php");

/** The Model is the class holding data about a collection of books. 
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{        
    /**
      * The PDO object for interfacing the database
      *
      */
    protected $db = null;  
    
    /**
     * @throws PDOException
     */
    public function __construct($db = null)  
    {  
        $user='root';
        $password='';
        $hostname="localhost";
        if ($db) 
        {
            $this->db = $db;
        }
        else
        {
           $this->db  = new PDO( 'mysql:host=localhost;dbname=test;charset=utf8mb4',$user,$password) ;// Create PDO connection
           $this->db ->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

        }
    }
    
    /** Function returning the complete list of books in the collection. Books are
     * returned in order of id.
     * @return Book[] An array of book objects indexed and ordered by their id.
     * @throws PDOException
     */
   public function getBookList()
    {
        try
        {
            $booklist = array();
            
            $sql = "SELECT * FROM book ORDER BY id";
            $query = $this->db->prepare($sql);
            $query->execute();
            $row = $query->fetchAll(PDO::FETCH_OBJ); 
            if($row){
                $booklist=$row;
            }
            return $booklist;
            }

        catch(PDOException $e){
            echo 'could not get gooklist' . $e->getMessage();
            return false;
            }
    }


 
public function getBookById($id)
    {
        $book = NULL;
        try{

            if(is_numeric($id))
            {
            $sql = "SELECT * FROM book WHERE id = $id";
            $query = $this->db->prepare($sql);
            $query->execute();
            $row = $query->fetch(PDO::FETCH_OBJ);
            if($row) {
                    $book = $row;
                return $book;
            }
             } else{return NULL;}
        }
        catch(PDOException $e) 
        {
         echo 'could not get book by id' .  $e->getMessage();  
         return NULL ;
        }
    }

        public function addBook($book)
    {
        try{ 
           
            if(!empty($book->title) && !empty($book->author)) 
            {
                $sql = "INSERT INTO book(title, author, description) VALUES(:title, :author, :description)";
                $query = $this->db->prepare($sql);

                $query->execute(array(":title"=>$book->title,":author"=>$book->author,
                    ":description"=>$book->description));
                    $book->id=1;          
                     return true ;
            } else{return false;}
        }
        catch(PDOException $e)
        {
             echo 'could not add '.  $e->getMessage(); return false;
           
        }
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using <PDO and a real database.
     */
    public function modifyBook($book)
    {

try {
        if(!empty($book->title) && !empty($book->author)) {

            $sql = "UPDATE book SET title = ?, author = ?, description = ? WHERE id = ?";

            $query = $this->db->prepare($sql);
            $query->execute(array($book->title, $book->author,
                                  $book->description, $book->id));
            return true;
        }else{ return false;}
    }
    catch(PDOException $e)
    { echo 'could not modify '.  $e->getMessage(); return false;}
       
}
    

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {

        try {
        $sql = "DELETE FROM book WHERE id=$id";
        $query=$this->db->prepare($sql);
        $query->execute(array($sql));
    } catch (PDOException $e) {
        $e->getMessage();
        return false;
      }
    }

       
}



?>